var CryptoJS = require('crypto-js');
var https = require('https');
var fs = require('fs');
var cases = require('../config/cases.json');


/*
*
* */
class Test {

    // 加密
    static encrypt(str) {
        var key = CryptoJS.enc.Utf8.parse('UITN25LMUQC436IM');
        var iv = CryptoJS.enc.Utf8.parse('8NONwyJtHesysWpM');
        var encrypted = CryptoJS.AES.encrypt(str, key, {iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7});
        return encrypted.toString();
    }

    // 解密
    static decrypt(str){
        var key = CryptoJS.enc.Utf8.parse('UITN25LMUQC436IM');
        var iv = CryptoJS.enc.Utf8.parse('8NONwyJtHesysWpM');
        var decrypted = CryptoJS.AES.decrypt(str, key, {iv: iv, padding: CryptoJS.pad.Pkcs7});
        return decrypted.toString(CryptoJS.enc.Utf8);
    }

    //
    static getTime(){
        var date = new Date().getTime().toString();
        return date.substr(0, date.length-3);
    }

    //
    static getDate(){
        var date = new Date();
        var year = date.getFullYear();
        var month = date.getMonth() < 10 ?  '0'+ date.getMonth(): date.getMonth();
        var day = date.getDate() < 10 ?  '0'+ date.getDate(): date.getDate();
        var hour = date.getHours() < 10 ?  '0'+ date.getHours(): date.getHours();
        var minute = date.getMinutes() < 10 ?  '0'+ date.getMinutes(): date.getMinutes();
        var second = date.getSeconds() < 10 ?  '0'+ date.getSeconds(): date.getSeconds();
        return `${year}-${month}-${day} ${hour}:${minute}:${second}`;
    }

    //
    static reqCases(name, ops, data){
        return new Promise(resolve => {
           var headers = JSON.stringify(ops);
            var req = https.request(ops, (res) => {
                res.on('data', (chunk) => {
                    var date = this.getDate();
                    this.writerFile(`${cases.logDir}test-${date.substr(0,10)}.log`, `\n${date}\n${name}\n${headers}\n${data}\n${chunk}\n`);
                    var res = JSON.parse(chunk.toString());
                    if (name === '获取-token' || name === '登录-密码登录'){
                            this.token = res.data.token;
                    }else {
                        this.transfer = res.data;
                    }
                    resolve(res);
                });
            });
            req.write(data);
            req.end();
        });
    }

    //
    static writerFile(fileName, data){
        try {
            fs.appendFile(fileName, data, (err) => {
                if (err) throw err;
            });
        }catch (e) {
            console.log(e);
        }
    }


    //
    static async run() {
        //
        try {
            fs.access('./test.log', fs.constants.F_OK, (err) => {
                if (!err) {
                    fs.unlink('./test.log', (err) => {
                        // if (err) throw err;
                    });
                }
            });
        }catch (e) {
            console.log(e);
        }

        var time = this.getTime();
        var ops = {
            hostname: cases.hostname,
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };

        for (var i=0; i<cases.cases.length; i++){
            var name = cases.cases[i].name;
            ops.path = cases.cases[i].path;
            var data = cases.cases[i].data;

            switch (name) {
                case '发帖-支付信息':
                    data.post_id = this.transfer;
                    break;
                case '发帖-微信支付':
                    data.post_id = this.transfer.post_id;
                    data.amount = this.transfer.post_money;
                    break;
                case '充值-E币微信支付':
                    data.recharge_id = this.transfer.recharge_table[0].recharge_id;
                    data.amount = this.transfer.recharge_table[0].rmb;
                    break;
                case '充值-个人vip微信支付':
                    data.recharge_id = "1";
                    data.amount = this.transfer['1'].rmb;
                    break;
                case '购买-商品微信支付':
                    // data.trade_no = this.transfer;
                    break;
                case '待付款-商品微信支付':
                    data.trade_no = this.transfer.trade_no;
                    break;
                default:
                    break;
            }

            data.time = time;

            data.sign = this.encrypt(JSON.stringify(data));
            if (this.token !== undefined){
                data.token = this.token;
            }
            await this.reqCases(name, ops, JSON.stringify(data));
        }

    }
}
module.exports = Test;

